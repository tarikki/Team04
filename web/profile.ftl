<!DOCTYPE html>
<html>
<head>
    <title>Dare2Date</title>
</head>
<body>

<#include "/header.ftl">
<link rel="stylesheet" type="text/css" href="dareProfile.css"
">


<!-- browse pictures from computer? then save somehow -->

<!-- call profile picture here instead of default picture if picture has been changed from default -->

<div class="container" align="center">

    <br>

    <img src="${picture}" id="profilepic" alt="../resources/pictures/defaultprofile.png">


    <!-- save picture -->
<#if ownProfile>
    <form class="pics" action="uploadServlet" method="post" enctype="multipart/form-data">
        <ul>
            <li><input type="file" name="profilepicture"></li>
            <li><input type="submit" Value="Upload"></li>
        </ul>
    </form>
</#if>

    <h2 class="userName">${userProfile.getUserName()}</h2>
    <br/>
    <#if !ownProfile><a id="mailme" href="/mailbox?messageTo=${userProfile.getUserName()}">Message me ;)</a></#if>
    <br/>

    <h2 class="info">Personal info</h2>

    <form class="edits" action="" method="post">
        <ul>
            <li>
                <label for="age">Age:</label>
                <input type="text" name="age" id="age" value="${userProfile.age}" disabled>
            <#if ownProfile> <a href=""
                                onclick="document.getElementById('age').disabled=false; return false;">edit</a> </#if>
            </li>

            <li>
                <label for="email">E-mail:</label>
                <input type="email" name="email" id="email" value="${userProfile.getEmail()}" disabled>
            <#if ownProfile>  <a href=""
                                 onclick="document.getElementById('email').disabled=false; return false;">edit</a></#if>
            </li>

            <li>
                <label for="gender">Gender:</label>

                <input type="text" name="gender" id="gender" disabled value="${userProfile.gender}">

            </li>

            <li>
                <label for="interestedIn">Interested in:</label>


                <input type="text" name="interestedIn" disabled value="${userProfile.getInterestedIn()}">


            </li>

            <li>
                <label for="location">Location:</label>

                <input type="text" name="location" id="location" value="${userProfile.getLocation()}" disabled>
            <#if ownProfile>  <a href=""
                                 onclick="document.getElementById('location').disabled=false; return false;">edit</a></#if>
            </li>

            <li>
                <label for="hobby">Hobbies:</label>

                <select name="hobby" id="hobby" disabled>
                    <option value="none" <#if userProfile.getHobbies() == "none">selected="selected"</#if>>None</option>
                    <option value="sports" <#if userProfile.getHobbies() == "sports">selected="selected"</#if>>Sports
                    </option>
                    <option value="arts" <#if userProfile.getHobbies() == "arts">selected="selected"</#if>>Arts</option>
                    <option value="computers" <#if userProfile.getHobbies() == "computers">selected="selected"</#if>>
                        Computers
                    </option>
                </select>
            <#if ownProfile><a href=""
                               onclick="document.getElementById('hobby').disabled=false; return false;">edit</a></#if>
            </li>


            <li>
                <label for="music">Music:</label>


                <select name="music" id="music" name="music" disabled>
                    <option value="none" <#if userProfile.getMusic() == "none">selected="selected"</#if>>None</option>
                    <option value="heavy" <#if userProfile.getMusic() == "heavy">selected="selected"</#if>>Heavy metal
                    </option>
                    <option value="classical" <#if userProfile.getMusic() == "classical">selected="selected"</#if>>
                        Classical music
                    </option>
                    <option value="sheepsong" <#if userProfile.getMusic() == "sheepsong">selected="selected"</#if>>Shaun
                        the Sheep theme song
                    </option>
                </select>

            <#if ownProfile><a href=""
                               onclick="document.getElementById('music').disabled=false; return false;">edit</a></#if>
            </li>

            <li>
                <label for="vip">VIP:</label>


                <input name="vip" type="radio" id="vip" value="true"
                       <#if userProfile.isVIP()>checked="checked" </#if><#if !ownProfile>disabled</#if>>Yes
                <input name="vip" type="radio" value="false" <#if !userProfile.isVIP()>checked="checked" </#if><#if !ownProfile>disabled</#if>>No


            </li>


        </ul>

        <br>
    <#if ownProfile>
        <h2 class="info2">Payment information</h2>


        <li>
            <label for="creditCard">Credit card number:</label>
            <input type="text" name="creditcard" pattern="^[0-9]{16,19}"
                   title="Your credit card number length should be 16 to 19 digits.">
            <br>
            <input type="submit" id="save" value="Save">
        </li>
    </#if>


    </form>
</div>


</body>
</html>