<html>
<head>
    <title>Dare2Date</title>
    <link rel="stylesheet" type="text/css" href="dareHeader.css">
</head>
<body>


<div class="header">

    <h1>Dare2Date</h1>

    <nav id="links">
        <a href="/match">Find a match</a>
        <a href="/browse">Browse</a>

        <a href="/index">Home</a>

    </nav>

    <!-- form action should send the data to the login server instead of the action_page.php -->

<#if loggedIn??>
<#if loggedIn>
    <div id="user">
        <p id="welcome">Welcome, ${userName}!</p>

        <nav id="userlinks">
            <a href= "/profile?user=${userName}">Profile</a>
            <a href="/mailbox">Mailbox${unreadMessages}</a>
            <a href="/logout">Log out</a>
        </nav>
    </div>
<#---->
<#else>
    <form id="login" align="right" action="/login" method="post">
        User name: <input type="text" name="username" required="required">
        Password: <input type="password" name="password" required="required">
        <input type="submit" value="Login">
        <br>

        <p id="register">
            <a href="/registeration"><u>Not a member yet? Click here to register.</u></a>
        </p>
    </form>


</#if>

<#else>
    <form id="login" align="right" action="/login" method="post">
        User name: <input type="text" name="username" required="required">
        Password: <input type="password" name="password" required="required">
        <input type="submit" value="Login">
        <br>

        <p id="register">
            <a href="/registeration"><u>Not a member yet? Click here to register.</u></a>
        </p>
    </form>
</#if>

</div>


</body>
</html>