<!DOCTYPE html>
<html>

<head>
    <title>Dare2Date</title>
</head>
<body>
<#include "/header.ftl">
<link rel="stylesheet" type="text/css" href="dareMatch.css">


<div class="container" align="center">
<h2>Matcher</h2>

<#if VIP == true>
    <#if blindMatch??>
    <p>Your best match based on ${matchMethod} is: ${blindMatch.getUserName()}</p>
    <br>
    <table>
        <tr>
            <td>User:</td>
            <td><a href="/profile?user=${blindMatch.getUserName()}">${blindMatch.getUserName()}</a></td>
        </tr>
        <tr>
            <td>Age:</td>
            <td>${blindMatch.getAge()}</td>
        </tr>
        <tr>
            <td>Location:</td>
            <td>${blindMatch.getLocation()}</td>
        </tr>
    </table>

    <#else >
    <p>Find a date based on: </p>

    <p>(Select only one)</p>

    <form action="/match" method="post">
    <#----    Age from <input type="text" maxlength="2" size="2" name="ageMinMatch"> to <input type="text" maxlength="2"
                                                                                         size="2"
                                                                                         name="ageMaxMatch"><br>
    <#---->

        <div id="fields" align="center">
            <ul>
        <input type="checkbox" name="locationMatch" checked>Location<br>
        <input type="checkbox" name="hobbiesMatch">Hobbies<br>
        <input type="checkbox" name="musicMatch">Music<br>
</ul>
        </div>
        <br>
        <input type="submit" value="Match Me">

    </form>

    </#if>
<#else >
<p>Not VIP, your best completely random match is: ${blindMatch.getUserName()}</p>
<br>
<table>
    <tr>
        <td>User:</td>
        <td><a href="/profile?user=${blindMatch.getUserName()}">${blindMatch.getUserName()}</a></td>
    </tr>
    <tr>
        <td>Age:</td>
        <td>${blindMatch.getAge()}</td>
    </tr>
    <tr>
        <td>Location:</td>
        <td>${blindMatch.getLocation()}</td>
    </tr>
</table>
</div>
</#if>


</body>

