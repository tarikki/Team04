<html>
<head>
    <title>Dare2Date</title>
</head>
<body>

<#include "/header.ftl">
<link rel="stylesheet" type="text/css" href="dareRegistration.css">


<!-- form action should send the data somewhere else -->


<h2>Registration Form</h2>

<div class="container" align="center">

    <form class="register" action="/registeration" method="post">

        <ul>
            <li>
                <label for="userName">Account name:</label>
                <input type="text" name="username" placeholder="Dare2Dater" required="required"
                       title="Enter your desired user name.">
            </li>

            <li>
                <label for="password">Password:</label>
                <input type="password" name="psw" placeholder="******" required="required"
                       pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}" title="Your password should be at l
                       east 6 characters long and contain at least one
                       lower case letter, one upper case letter and one digit. eg. Dikke1">

            </li>

            <li>
                <label for="firstName">First name:</label>
                <input type="text" name="firstname" placeholder="John" required="required"
                       pattern="^[a-zA-Z]+$" title="Enter your first name. Note that it can not contain numbers!">
            </li>

            <li>
                <label for="lastName">Last name:</label>
                <input type="text" name="lastname" placeholder="Doe" required="required"
                       pattern="^[a-zA-Z]+$" title="Enter your last name. Note that it can not contain numbers!">
            </li>

            <li>
                <label for="email">Email:</label>
                <input type="email" name="email" placeholder="user@dare2date.nl" required="required"
                       title="Enter your e-mail address.">
            </li>

            <li>
                <label for="age">Age:</label>
                <input type="text" name="age" id="age" placeholder="18" required="required" pattern="^\d{2}"
                       title="Your age should be only 2 digits.">
            </li>

            <li>
                <label for="city">City:</label>
                <input type="text" name="city" placeholder="Amsterdam" required="required" pattern="^[a-zA-Z]+$"
                       title="Enter your the city you live in.">
            </li>

            <li>
                <label for="Gender">Gender:</label>
                <select name="gender" required="required">
                    <option value="man">Man</option>
                    <option value="woman">Woman</option>
                </select>
            </li>

            <li>
                <label for="interestedIn">Interested in:</label>
                <select name="interested_in" required="required">
                    <option value="men">Men</option>
                    <option value="women">Women</option>
                    <option value="sheep">Sheep</option>
                </select>
            </li>

            <li>
                <label for="creditCard">Credit card number:</label>
                <input type="text" name="creditcard" placeholder="123456789" required="required" pattern="^[0-9]{16,19}"
                       title="Your credit card number length should be 16 to 19 digits.">


        </ul>
        <br>
        <br>
        <br>
        <input type="submit" value="Register">
    </form>
</div>


</body>
</html>