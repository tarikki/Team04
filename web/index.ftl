<!DOCTYPE html>
<html>
<link rel="stylesheet" href="dareindex.css"/>

<head>
    <title>Dare2Date</title>
</head>
<body>
<#include "/header.ftl">
<div name="title" align="center">
<h2>A Selection of Our Users</h2>
</div>

<div class="selectedUsers">
<#list selectedUsers as user>
    <div class="user">
        <img <#if loggedIn??>
            <#if loggedIn = true>src="${pictures_directory}${user.getPictureName()}"
            <#else>
            src="../resources/pictures/defaultprofile.png"</#if>
        <#else>
            src="../resources/pictures/defaultprofile.png"</#if>
            alt="Log in to see the pictures!"
            style="width: 185px; height: 185px"/>

        <p>User: ${user.getUserName()}</p>

        <p>Gender: ${user.getGender()}</p>

        <p>Age: ${user.getAge()}</p>

        <p>Interested in: ${user.getInterestedIn()}</p>
        <#if loggedIn>        <a href="/profile?user=${user.getUserName()}"> <#else>  <a href=""
                                                                                         onclick="registerAlert()"></#if>
        <span
                class="link-span"></span></a>
        <script>
            function registerAlert() {
                window.alert("Login to see moar!!!")
            }
        </script>

    </div>
</#list>
</div>


</body>
</html>