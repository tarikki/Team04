<!DOCTYPE html>
<html>
<head>
    <title>Dare2Date</title>
<#include "/header.ftl">
    <link rel="stylesheet" type="text/css" href="dareMailBox.css">
</head>
<body>

<h2>Inbox</h2>
<#if messageTo??><h2><a href="/profile?user=${messageTo}">${messageTo}</a></h2></#if>

<div class="senderList">
    <p id="from">From: </p>
    <table >
    <#if messageChains??>
        <#if messageChains?keys?size gt 0>
            <#list messageChains?keys as chain>
                <tr>
                    <th>

                        <a href="/mailbox?user=${chain}">${chain}
                            <#if messageChains[chain].getUnreadMessagesNumber() gt 0>
                                (${messageChains[chain].getUnreadMessagesNumber()})</#if>
                        </a>
                    </th>
                </tr>
            </#list>

        <#else>


                   <p id ="noMail">Sorry, your mailbox is empty.</p>



        </#if>
    </#if>

    </table>
</div>

<div class="messageFields-holder">
    <div class="oldMsg">
        <textarea name="oldMessages" id="oldMessages" cols="30" readonly="readonly" rows="20">${oldMessages}</textarea>
    </div>


    <div class="reply"><p>Message to: </p>

        <form action="/mailbox" id="messageForm" method="post"><input type="text" size="10" name="messageTo" required="required"
                                                                      <#if messageTo??>value="${messageTo}</#if>">
            <input type="submit" id="sendButton" value="Send"></form>

        <textarea name="content" id="content" cols="30" rows="10" form="messageForm"></textarea>
    </div>
</div>

</body>
</html>
