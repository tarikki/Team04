package controller;

import matching.BlindMatchInArea;
import matching.BlindMatchWithHobbies;
import matching.BlindMatchWithMusic;
import matching.CompletelyBlindMatch;
import model.User;
import util.MessageLoader;
import util.UserReader;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashMap;

/**
 * Created by extradikke on 22/03/15.
 */
@WebServlet(name = "Search")
public class MatchPage extends HttpServlet {


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        /**this can go to any block that needs current username**/
        boolean loggedIn;
        HttpSession session = request.getSession();

        System.out.println("loading list from disk");
        UserReader userReader = new UserReader();
        HashMap<String, User> userList = userReader.readAllUsers();


        if (session.getAttribute("loggedIn") != null) {
            loggedIn = (Boolean) session.getAttribute("loggedIn");
            if (loggedIn) {
                String currentUserName = (String) session.getAttribute("userName");
                User currentUser = userList.get(currentUserName);
                System.out.println(currentUser.getUserName() + " VIP" + currentUser.isVIP());
                if (currentUser.isVIP()) {
                    request.setAttribute("VIP", true);


                } else {
                    request.setAttribute("VIP", false);
                    CompletelyBlindMatch completelyBlindMatch = new CompletelyBlindMatch();
                    User blindMatch = completelyBlindMatch.match(currentUser, userList);
                    request.setAttribute("blindMatch", blindMatch);
                }
                request.setAttribute("unreadMessages", MessageLoader.countUnreadMail(currentUserName));
                request.getRequestDispatcher("/matchPage.ftl").forward(request, response);
            }
        } else {
            response.sendRedirect("/index");
        }

    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        boolean loggedIn;
        HttpSession session = request.getSession();
        System.out.println("loading list from disk");
        UserReader userReader = new UserReader();
        HashMap<String, User> userList = userReader.readAllUsers();
        String currentUserName = (String) session.getAttribute("userName");
        User currentUser = userList.get(currentUserName);

        String location = request.getParameter("locationMatch");
        String hobbies = request.getParameter("hobbiesMatch");
        String music = request.getParameter("musicMatch");

        if (currentUser.isVIP()) {
            request.setAttribute("VIP", true);


        } else {
            request.setAttribute("VIP", false);
        }

        request.setAttribute("unreadMessages", MessageLoader.countUnreadMail(currentUserName));

        boolean redirectedAlready = false;

        if (location != null && !redirectedAlready) {
            BlindMatchInArea blindMatchInArea = new BlindMatchInArea();

            User blindMatch = blindMatchInArea.match(currentUser, userList);
            request.setAttribute("matchMethod", "location");
            request.setAttribute("blindMatch", blindMatch);
            redirectedAlready = true;
            request.getRequestDispatcher("/matchPage.ftl").forward(request, response);

        }

        if (hobbies != null && !redirectedAlready) {
            BlindMatchWithHobbies blindMatchWithHobbies = new BlindMatchWithHobbies();
            User blindMatch = blindMatchWithHobbies.match(currentUser, userList);
            request.setAttribute("matchMethod", "hobbies");
            request.setAttribute("blindMatch", blindMatch);
            redirectedAlready = true;
            request.getRequestDispatcher("/matchPage.ftl").forward(request, response);

        }

        if (music != null && !redirectedAlready) {
            BlindMatchWithMusic blindMatchWithMusic = new BlindMatchWithMusic();
            User blindMatch = blindMatchWithMusic.match(currentUser, userList);
            request.setAttribute("matchMethod", "music");
            request.setAttribute("blindMatch", blindMatch);
            redirectedAlready = true;
            request.getRequestDispatcher("/matchPage.ftl").forward(request, response);

        }


        System.out.println(location);
        System.out.println(hobbies);
        System.out.println(music);


    }
}
