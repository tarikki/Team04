package controller;

import model.User;
import util.FilePaths;
import util.MessageLoader;
import util.UserReader;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

/**
 * Created by extradikke on 27/02/15.
 */
public class Browse extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        ServletContext context = request.getServletContext();
        System.out.println("loading list from disk");
        UserReader userReader = new UserReader();
        HashMap<String, User> userList = userReader.readAllUsers();
        HttpSession session = request.getSession();
        System.out.println(session.getId());
        if (session.getAttribute("loggedIn") != null) {
            Boolean loggedIn = (Boolean) session.getAttribute("loggedIn");
            if (loggedIn) {
                System.out.println(session.getAttribute("userName"));
            }

        }



        context.setAttribute("file-upload", FilePaths.PICTURES_DIRECTORY_MAIN);
        ArrayList<User> selectedUsers = new ArrayList<User>(3);
        Random random = new Random();
        int selectionSize = userList.size();
        Object[] userInArray = userList.values().toArray();

        for (int i = 0; i < selectionSize; i++) {
            int index = random.nextInt(selectionSize);
            System.out.println(index);
            boolean notDuplicate = false;
            User user = (User) userInArray[random.nextInt(userInArray.length)];
            while (!notDuplicate){
                user = (User) userInArray[random.nextInt(userInArray.length)];
                if (!selectedUsers.contains(user)){
                    notDuplicate = true;
                }
            }
            selectedUsers.add(user);
        }

        request.setAttribute("selectedUsers", selectedUsers);
        boolean loggedIn = false;
        if (session.getAttribute("loggedIn") != null) {
            loggedIn = (Boolean) session.getAttribute("loggedIn");
            if (loggedIn) {
                String userName = (String) session.getAttribute("userName");
                request.setAttribute("userName", userName);

                request.setAttribute("unreadMessages", MessageLoader.countUnreadMail(userName));
            }
        }

        request.setAttribute("loggedIn", loggedIn);
        request.setAttribute("pictures_directory", FilePaths.PICTURES_DIRECTORY_HTML);
        response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
        response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
        response.setDateHeader("Expires", 0); // Proxies.
        request.getRequestDispatcher("/index.ftl").forward(request, response);

    }






}
