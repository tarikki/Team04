package controller;

import model.User;
import util.MessageLoader;
import util.SaveUser;
import util.UserReader;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by extradikke on 27/02/15.
 */
public class Registration extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        System.out.println(session.getId());
        request.setAttribute("unreadMessages", MessageLoader.countUnreadMail(" "));
        request.getRequestDispatcher("/registeration.ftl").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ServletContext context = request.getServletContext();

        System.out.println("loading list from disk");
        UserReader userReader = new UserReader();
        HashMap<String, User> userList = userReader.readAllUsers();
        String firstname = request.getParameter("firstname");
        String lastname = request.getParameter("lastname");
        String username = request.getParameter("username");
        String password = request.getParameter("psw");
        String email = request.getParameter("email");
        System.out.println(email);
        int age = Integer.valueOf(request.getParameter("age"));
        String gender = request.getParameter("gender");
        String city = request.getParameter("city");
        String interestedIn = request.getParameter("interested_in");
        String creditcard = request.getParameter("creditcard");
        SaveUser saveUser = new SaveUser();
        boolean VIP = false;
        saveUser.saveUser(username, password, firstname, lastname, email, age, city, gender, interestedIn, creditcard, "None", "None", VIP, userList);
        System.out.println(gender);
        System.out.println(interestedIn);
        System.out.println(firstname + " " + lastname + " " + username + " " + password + " " + email + " " + creditcard);
        doGet(request, response);
    }
}
