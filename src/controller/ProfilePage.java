package controller;

import model.User;
import util.*;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashMap;

/**
 * Created by extradikke on 01/03/15.
 */
public class ProfilePage extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        String currentUser = (String) session.getAttribute("userName");
        ServletContext context = request.getServletContext();
        String requestedUser = request.getParameter("user");
        System.out.println(requestedUser);
        System.out.println("loading list from disk");
        UserReader userReader = new UserReader();
        HashMap<String, User> userList = userReader.readAllUsers();
        request.setAttribute("ownProfile", false);
        if (userList.containsKey(requestedUser)) {
            System.out.println("found");
            if (currentUser.equals(requestedUser)) {
                request.setAttribute("ownProfile", true);
            }
        }
        request.setAttribute("userProfile", userList.get(requestedUser));
        request.setAttribute("pictureLocation", FilePaths.PICTURES_DIRECTORY_MAIN);
        System.out.println(FilePaths.PICTURES_DIRECTORY_MAIN);


        String picture = PictureFinder.getPicturePath(requestedUser);
        request.setAttribute("picture", FilePaths.PICTURES_DIRECTORY_HTML + picture);
        request.setAttribute("unreadMessages", MessageLoader.countUnreadMail(currentUser));
        response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
        response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
        response.setDateHeader("Expires", 0); // Proxies.

        request.getRequestDispatcher("/profile.ftl").forward(request, response);

//        if (session.getAttribute("userName"))

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        String currentUserName = (String) session.getAttribute("userName");

        System.out.println("loading list from disk");
        UserReader userReader = new UserReader();
        HashMap<String, User> userList = userReader.readAllUsers();
        User currentUser = userList.get(currentUserName);
        String email = request.getParameter("email");
        String age = request.getParameter("age");
        String city = request.getParameter("location");
        String music = request.getParameter("music");
        String hobby = request.getParameter("hobby");
        String vip = request.getParameter("vip");

        if (email != null) {
            currentUser.setEmail(email);
        }
        if (city != null) {
            currentUser.setLocation(city);
        }

        if (age != null) {
            currentUser.setAge(Integer.valueOf(age));
        }

        if (hobby != null) {
            currentUser.setHobbies(hobby);
        }

        if (music != null) {
            currentUser.setMusic(music);
        }

        if (vip != null) {
            currentUser.setVIP(Boolean.valueOf(vip));
        }

        System.out.println(hobby + " " + music + " " + vip);
        System.out.println(email + " " + age + " " + city);
        SaveUser saveUser = new SaveUser();
        saveUser.editProfile(currentUser);
        System.out.println("/profile?user=" + currentUserName);
        response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
        response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
        response.setDateHeader("Expires", 0); // Proxies.
        response.sendRedirect("/profile?user=" + currentUserName);

    }
}
