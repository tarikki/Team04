package controller;

import model.MessageChain;
import model.User;
import util.LoginValidator;
import util.MessageLoader;
import util.UserReader;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by extradikke on 05/03/15.
 */
public class Login extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        boolean loggedIn = false;

        if (session.getAttribute("loggedIn") != null) {
            loggedIn = (Boolean) session.getAttribute("loggedIn");
            if (loggedIn) {
                String userName = (String) session.getAttribute("userName");
                request.setAttribute("userName", userName);

            }
        }
        request.getRequestDispatcher("/login.ftl").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String userName = request.getParameter("username");
        String password = request.getParameter("password");

        HttpSession session = request.getSession();
        System.out.println(session.getId());
        System.out.println("loading list from disk");
        UserReader userReader = new UserReader();
        HashMap<String, User> userList = userReader.readAllUsers();
        System.out.println(userName + " " + password);
        System.out.println("login attempted");
        String unreadString = " ";
        if (LoginValidator.isValidLogin(userName, password, userList)) {
            System.out.println("login correct!");
            session.setAttribute("userName", userName);
            session.setAttribute("loggedIn", true);

        }
        request.setAttribute("unreadMessages", unreadString);
        response.sendRedirect("/index");
    }




}
