package controller;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FileUtils;
import util.FilePaths;
import util.ImageScale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.util.Iterator;
import java.util.List;


/**
 * Created by extradikke on 14/03/15.
 */
public class UploadServlet extends HttpServlet{
    private String filePath;
    private boolean isMultipart;
    private int maxFileSize = 5000 * 1012;
    private int maxMemSize = 4 * 1024;
    private File fileMain;
    private File fileServer;
    private String userName;

    public void init(){
        filePath = (String) getServletContext().getAttribute("fileMain-upload-");

    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException {
        HttpSession session = request.getSession();
        userName = (String) session.getAttribute("userName");
        isMultipart = ServletFileUpload.isMultipartContent(request);
        if(!isMultipart){
            System.out.println("multipart, but why important?");
            return;
        }

        DiskFileItemFactory factory = new DiskFileItemFactory();
        factory.setSizeThreshold(maxMemSize);
        factory.setRepository(new File(FilePaths.TEMP_DIRECTORY));
        ServletFileUpload upload = new ServletFileUpload(factory);
        upload.setSizeMax(maxFileSize);

        try{
            List fileItems = upload.parseRequest(request);

            Iterator iterator = fileItems.iterator();

            while (iterator.hasNext()){
                FileItem fileItem = (FileItem)iterator.next();
                if(!fileItem.isFormField()){
                    // parameters
                    String fieldName = fileItem.getFieldName();
                    String fileName = fileItem.getName();
                    String contentType = fileItem.getContentType();
                    String[] fileExtension = contentType.split("/");
                    boolean isInMemory = fileItem.isInMemory();
                    long sizeInBytes = fileItem.getSize();
                    System.out.println(fieldName +" "+ fileName+" "+  contentType+" "+  isInMemory+" "+  sizeInBytes + " filename: " + userName+"."+fileExtension[1]);
                    // Write fileMain
                    fileMain = new File(FilePaths.TEMP_DIRECTORY+userName+"."+fileExtension[1]);
//                    fileServer = new File(FilePaths.PICTURES_DIRECTORY_SERVER+userName+"."+fileExtension[1]);
                    System.out.println(fileServer);
//                    if (fileName.lastIndexOf("\\") >= 0){
//                        fileMain = new File(filePath + fileName.substring(fileName.lastIndexOf("\\")));
//                    }else {
//                        fileMain = new File(filePath + fileName.substring(fileName.lastIndexOf("\\")+1));

//                    }
                    fileItem.write(fileMain);
                    ImageScale.scale(userName + "." + fileExtension[1]);
                    System.out.println("Uploaded fileMain: " + fileName);
                    response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
                    response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
                    response.setDateHeader("Expires", 0); // Proxies.
                response.sendRedirect("/profile?user="+userName);
                }

            }
        } catch (Exception e){
            e.printStackTrace();
        }
    }








}
