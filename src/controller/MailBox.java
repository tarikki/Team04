package controller;

import model.MailMessage;
import model.MessageChain;
import model.User;
import util.MessageLoader;
import util.MessageSaver;
import util.UserReader;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashMap;

/**
 * Created by extradikke on 19/03/15.
 */
public class MailBox extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        /**this can go to any block that needs current username**/
        boolean loggedIn;
        HttpSession session = request.getSession();
        ServletContext context = request.getServletContext();
        System.out.println("loading list from disk");
        UserReader userReader = new UserReader();
        HashMap<String, User> userList = userReader.readAllUsers();


        if (session.getAttribute("loggedIn") != null) {
            loggedIn = (Boolean) session.getAttribute("loggedIn");
            if (loggedIn) {
                String messageTo = request.getParameter("messageTo");
                String requestedUser = request.getParameter("user");
                String oldMessages = "";
                if (requestedUser != null) {
                    messageTo = requestedUser;
                }
                String currentUserName = (String) session.getAttribute("userName");
                request.setAttribute("currentUser", currentUserName);
                /** get messages **/
                HashMap<String, MessageChain> messageChains = MessageLoader.loadMessages(currentUserName);
                System.out.println(messageChains.size());
                if (messageChains != null) {
                    if (messageChains.containsKey(requestedUser)) {
                        MessageChain requestedChain = messageChains.get(requestedUser);
                        requestedChain.allMessagesRead();
                        messageChains.put(requestedUser, requestedChain);
                        MessageSaver.saveMessages(currentUserName, messageChains);
                        oldMessages = messagesToString(requestedChain);
                    }
                }

                request.setAttribute("messageChains", messageChains);
                request.setAttribute("oldMessages", oldMessages);
                request.setAttribute("messageTo", messageTo);
                request.setAttribute("unreadMessages", MessageLoader.countUnreadMail(currentUserName));
                response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
                response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
                response.setDateHeader("Expires", 0); // Proxies.
                request.getRequestDispatcher("/mailbox.ftl").forward(request, response);
            }
        }
        /** ends here**/

        else {
            response.sendRedirect("/index");
        }


    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String content = request.getParameter("content");
        String messageTo = request.getParameter("messageTo");
        System.out.println(messageTo);
        HttpSession session = request.getSession();
        String currentUser = (String)session.getAttribute("userName");

        MailMessage mailMessage = new MailMessage(content, messageTo,currentUser);
//TODO check if user exists
        MessageSaver.addMessageToChain(messageTo, mailMessage);
        MessageSaver.addMessageToChain(currentUser, mailMessage);
        response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
        response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
        response.setDateHeader("Expires", 0); // Proxies.
        response.sendRedirect("/mailbox?user="+messageTo);


    }

    private String messagesToString(MessageChain messageChain) {
        StringBuilder stringBuilder = new StringBuilder();
        System.out.println(System.getProperty("line.separator"));
        for (MailMessage mailMessage : messageChain.getMailMessages()) {
            stringBuilder.append("\n");
//            stringBuilder.append("\n");
            stringBuilder.append(mailMessage.getSender());
            stringBuilder.append("(" + mailMessage.getTimeStamp()+ "):");
            stringBuilder.append(mailMessage.getContent());
            stringBuilder.append("\n");
            stringBuilder.append("***********");
        }

        return stringBuilder.toString();

    }
}
