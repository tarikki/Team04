package tester;

import org.imgscalr.Scalr;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Created by extradikke on 14/03/15.
 */
public class ImgscalarTester {

    public static void main(String[] args) throws IOException {
        BufferedImage image = ImageIO.read(new File("/media/extradikke/UbuntuData/programming/java/dare2date2/resources/pictures/Asda.jpeg"));
        BufferedImage thumbNail = Scalr.resize(image, Scalr.Method.SPEED, 150);
        File outputfile = new File("/media/extradikke/UbuntuData/programming/java/dare2date2/resources/pictures/tuuber.jpeg");
        ImageIO.write(thumbNail, "jpeg", outputfile);

    }
}
