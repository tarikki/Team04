package tester;

import matching.*;
import model.User;
import org.junit.Test;
import util.UserReader;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import static org.junit.Assert.*;


/**
 * Created by extradikke on 27/03/15.
 */
public class MatchTester {

    private UserReader userReader;
    private HashMap<String, User> userList;
    private Random random = new Random();
    private ArrayList<User> matches = new ArrayList<>();


    public MatchTester() {
        try {
            userReader = new UserReader();
            userList = userReader.readAllUsers();

            for (Map.Entry<String, User> dikke : userList.entrySet()) {

                matches.add(dikke.getValue()); /// If the cities match add the user to our match-list
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void testAll() {
        for (User user : matches) {
            System.out.println("Testing for " + user.getUserName());
            blindMatchInArea(user);
            blindMatchWithHobbies(user);
            blindMatchWithMusic(user);
            completelyBlindMatch(user);
        }
    }


    @Test
    public void blindMatchInArea(User currentUser) {

        BlindMatchInArea blindMatchInArea = new BlindMatchInArea();

        assertTrue("User not returned", blindMatchInArea.match(currentUser, userList) != null);
        assertNotSame("User can't be the same", currentUser, blindMatchInArea.match(currentUser, userList));

    }

    @Test
    public void blindMatchWithHobbies(User currentUser) {

        BlindMatchWithHobbies blindMatchWithHobbies = new BlindMatchWithHobbies();

        assertTrue("User not returned", blindMatchWithHobbies.match(currentUser, userList) != null);
        assertNotSame("User can't be the same", currentUser, blindMatchWithHobbies.match(currentUser, userList));

    }

    @Test
    public void blindMatchWithMusic(User currentUser) {

        BlindMatchWithMusic blindMatchWithMusic = new BlindMatchWithMusic();

        assertTrue("User not returned", blindMatchWithMusic.match(currentUser, userList) != null);
        assertNotSame("User can't be the same", currentUser, blindMatchWithMusic.match(currentUser, userList));

    }

    @Test
    public void completelyBlindMatch(User currentUser) {

        CompletelyBlindMatch completelyBlindMatch = new CompletelyBlindMatch();

        assertTrue("User not returned", completelyBlindMatch.match(currentUser, userList) != null);
        assertNotSame("User can't be the same", currentUser, completelyBlindMatch.match(currentUser, userList));

    }


}
