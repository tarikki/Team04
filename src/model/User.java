package model;

import util.PictureFinder;

import java.util.ArrayList;
import java.util.UUID;

/**
 * Created by Klara on 24.2.2015.
 */
public class User {

    private String userName;
    private String firstName;
    private String lastName;
    private int age;
    private String location;
    private String gender;
    private String email;
    private String interestedIn;
    private String creditcard;
    private String password;
    private boolean VIP;
    private String hobbies;
    private String music;
    public String pictureName;

    public String getPictureName() {
        return PictureFinder.getPicturePath(userName);
    }

    public void setPictureName(String pictureName) {
        this.pictureName = pictureName;
    }

    public String getHobbies() {
        return hobbies;
    }

    public void setHobbies(String hobbies) {
        this.hobbies = hobbies;
    }

    public String getMusic() {
        return music;
    }

    public void setMusic(String music) {
        this.music = music;
    }

    public String getUserName() {
        return userName;
    }

    public int getAge() {
        return age;
    }

    public String getGender() {
        return gender;
    }

    public User(String userName, String password, String firstName, String lastName, String email, int age, String location, String gender, String interestedIn, String creditcard, String hobbies, String music, boolean VIP) {
        this.age = age;
        this.creditcard = creditcard;
        this.email = email;
        this.firstName = firstName;
        this.gender = gender;
        this.hobbies = hobbies;
        this.interestedIn = interestedIn;
        this.lastName = lastName;
        this.location = location;
        this.music = music;
        this.password = password;
        this.userName = userName;
        this.VIP = VIP;
    }

    public User(String userName, String password, String firstName, String lastName, String email, int age, String location, String gender, String interestedIn, String creditcard) {


        this.userName = userName;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.age = age;
        this.location = location;
        this.gender = gender;
        this.interestedIn = interestedIn;
        this.creditcard = creditcard;
        this.VIP = false;
        this.music = "none";
        this.hobbies = "none";


    }


    // Use this constructor for editing user profile
    public User(String email, int age, String location, String creditcard, boolean VIP, String hobbies, String music) {


        this.email = email;
        this.age = age;
        this.location = location;

        this.creditcard = creditcard;
        this.VIP = VIP;
        this.hobbies = hobbies;
        this.music = music;

    }


    public boolean isVIP() {
        return VIP;
    }

    public void setVIP(boolean VIP) {
        this.VIP = VIP;
    }

    public String getPassword() {
        return password;
    }


    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public void setAge(int age) {
        this.age = age;
    }


    public String getCreditcard() {
        return creditcard;
    }

    public void setCreditcard(String creditcard) {
        this.creditcard = creditcard;
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getInterestedIn() {
        return interestedIn;
    }

    public void setInterestedIn(String interestedIn) {
        this.interestedIn = interestedIn;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }


    public void setPassword(String password) {
        this.password = password;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Override
    public String toString() {
        return "User{" +
                "userName='" + userName + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", age=" + age +
                ", location='" + location + '\'' +
                ", gender='" + gender + '\'' +
                ", email='" + email + '\'' +
                ", interestedIn='" + interestedIn + '\'' +
                ", creditcard='" + creditcard + '\'' +
                ", password='" + password + '\'' +
                ", VIP=" + VIP +
                ", hobbies='" + hobbies + '\'' +
                ", music='" + music + '\'' +
                '}';
    }
}
