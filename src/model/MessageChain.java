package model;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by extradikke on 16/03/15.
 */
public class MessageChain {
    private String notOwner;
    private String sender;
    private String receiver;
    private Date lastMessageDate;
    private ArrayList<MailMessage> mailMessages;
    private boolean allMessagesRead = true;
    private int unreadMessages = 0;

    public MessageChain(MailMessage message, String notOwner) {
        this.notOwner = notOwner;
        this.receiver = message.getReceiver();
        this.sender = message.getSender();
        this.lastMessageDate = message.getTimeStamp();
        this.mailMessages = new ArrayList<MailMessage>();
        this.mailMessages.add(message);
    }

    public void newMessage(MailMessage message){
        this.lastMessageDate = message.getTimeStamp();
        this.mailMessages.add(message);
        if (!message.isRead()){
            allMessagesRead = false;
            unreadMessages++;
        }
    }

    public int getUnreadMessagesNumber() {
        return unreadMessages;
    }

    public void setUnreadMessages(int unreadMessages) {
        this.unreadMessages = unreadMessages;
    }

    public boolean areAllMessagesRead() {
        return allMessagesRead;
    }

    public void allMessagesRead() {
        this.allMessagesRead = true;
        this.unreadMessages = 0;
    }

    public String getNotOwner() {
        return notOwner;
    }

    public void setNotOwner(String notOwner) {
        this.notOwner = notOwner;
    }

    public Date getLastMessageDate() {
        return lastMessageDate;
    }

    public void setLastMessageDate(Date lastMessageDate) {
        this.lastMessageDate = lastMessageDate;
    }

    public ArrayList<MailMessage> getMailMessages() {
        return mailMessages;
    }

    public void setMailMessages(ArrayList<MailMessage> mailMessages) {
        this.mailMessages = mailMessages;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }
}
