package matching;

import model.User;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * Created by Veera on 27.3.2015.
 */
public class BlindMatchWithAll extends  MatcherAbstract implements Matcher {
    @Override
    public User match(User user, HashMap<String, User> userList) {
        matches = new ArrayList<>();

        super.convertStringsToRightFormat(user);

        for (Map.Entry<String, User> dikke : userList.entrySet()) {
            if (interestedIn.equalsIgnoreCase(dikke.getValue().getGender()) && user.getHobbies().equalsIgnoreCase(dikke.getValue().getHobbies()) && user.getMusic().equalsIgnoreCase(dikke.getValue().getMusic()) && user.getLocation().equalsIgnoreCase(dikke.getValue().getLocation())) {
                matches.add(dikke.getValue()); /// If all attributes match add to matches
            }
        }


        /// Remove the searcher from matches.
        super.removeUserFromResults(user, matches);

        if (!super.isEmpty(matches)) {

            return super.getRandomFromMatches();
        }
        return null;
    }

}
