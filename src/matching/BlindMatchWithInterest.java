package matching;

import model.User;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * Created by Veera on 27.3.2015.
 */
public class BlindMatchWithInterest extends MatcherAbstract implements Matcher {
    @Override
    public User match(User user, HashMap<String, User> userList) {
          /*
    Returns a random user who matches the searcher's interest
    eg. interested in = women -> find all women in userlist -> return a random woman
     */

        matches = new ArrayList<>();


        super.convertStringsToRightFormat(user);


        /// Get all males / females from the list
        for (Map.Entry<String, User> dikke : userList.entrySet()) {
            if (interestedIn.equalsIgnoreCase(dikke.getValue().getGender())) {
                matches.add(dikke.getValue()); /// If interest matches gender add it to list
            }
        }

        /// Remove the searcher from matches.
        super.removeUserFromResults(user, matches);

        if (!super.isEmpty(matches)) {


            return super.getRandomFromMatches();
        }
        return null;
    }
}
