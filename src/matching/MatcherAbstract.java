package matching;

import model.User;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

/**
 * Created by Veera on 27.3.2015.
 */
public abstract class MatcherAbstract {

    ArrayList<User> matches;
    Random random;
    String interestedIn;
    User blindMatch;

    public boolean isEmpty(ArrayList<User> matches) {
        if (matches.isEmpty()) {
            System.out.println("Sorry, no match found");
            return true;
        }
        return false;
    }

    public void removeUserFromResults(User user, ArrayList<User> matches) {
        matches.removeIf(searcher -> searcher.getUserName().equalsIgnoreCase(user.getUserName()));
    }

    /// Converting from interested in "men" to "man" so we can check if strings match
    public void convertStringsToRightFormat(User user) {

        if (user.getInterestedIn().equalsIgnoreCase("men")) {
            interestedIn = "man";
        } else if (user.getInterestedIn().equalsIgnoreCase("sheep")) {
            interestedIn = "sheep";
        } else {
            interestedIn = "woman";
        }
    }

    public User getRandomFromMatches() {
        random = new Random();
        blindMatch = matches.get(random.nextInt(matches.size()));
        return blindMatch;

    }

}
