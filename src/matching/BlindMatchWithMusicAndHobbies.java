package matching;

import model.User;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * Created by Veera on 27.3.2015.
 */
public class BlindMatchWithMusicAndHobbies extends MatcherAbstract implements Matcher {
    @Override
    public User match(User user, HashMap<String, User> userList) {
        matches = new ArrayList<>();


        for (Map.Entry<String, User> dikke : userList.entrySet()) {
            if (user.getHobbies().equalsIgnoreCase(dikke.getValue().getHobbies()) && user.getMusic().equalsIgnoreCase(dikke.getValue().getMusic())) {
                matches.add(dikke.getValue()); /// If both match
            }
        }


        /// Remove the searcher from matches.
        super.removeUserFromResults(user, matches);

        if (!super.isEmpty(matches)) {

            return super.getRandomFromMatches();
        }
        return null;
    }
}
