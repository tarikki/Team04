package matching;

import model.User;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * Created by Veera on 27.3.2015.
 */


    /*
    Returns a random user who lives in same city as the searcher
     */
public class BlindMatchInArea extends MatcherAbstract implements Matcher {
    @Override
    public User match(User user, HashMap<String, User> userList) {
        matches = new ArrayList<>();


        for (Map.Entry<String, User> dikke : userList.entrySet()) {
            if (user.getLocation().equalsIgnoreCase(dikke.getValue().getLocation())) {
                matches.add(dikke.getValue()); /// If the cities match add the user to our match-list
            }
        }


        /// Remove the searcher from matches.
        super.removeUserFromResults(user, matches);


        if (!super.isEmpty(matches)) {


            return super.getRandomFromMatches();

        }
        return null;
    }
}
