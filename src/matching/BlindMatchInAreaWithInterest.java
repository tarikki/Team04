package matching;

import model.User;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * Created by Veera on 27.3.2015.
 */

  /*
    Returns a random user who matches the searcher's interest and lives in the same city
    eg. interested in = women, city = Amsterdam -> find all women from Amsterdam -> return a random woman from Amsterdam
     */
public class BlindMatchInAreaWithInterest extends MatcherAbstract implements Matcher {
    @Override
    public User match(User user, HashMap<String, User> userList) {
        matches = new ArrayList<>();

        super.convertStringsToRightFormat(user);

        /// Get all males / females from the list
        for (Map.Entry<String, User> dikke : userList.entrySet()) {
            if (user.getLocation().equalsIgnoreCase(dikke.getValue().getLocation()) && interestedIn.equalsIgnoreCase(dikke.getValue().getGender())) {
                matches.add(dikke.getValue()); /// If interest matches gender add it to list
            }
        }


        /// Remove the searcher from matches.
        super.removeUserFromResults(user, matches);

        if (!super.isEmpty(matches)) {

            return super.getRandomFromMatches();
        }
        return null;
    }
}
