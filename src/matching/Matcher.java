package matching;

import model.User;

import java.util.HashMap;

/**
 * Created by Veera on 27.3.2015.
 */
public interface Matcher {



    public User match(User user, HashMap<String, model.User> userList);


}
