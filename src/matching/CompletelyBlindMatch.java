package matching;

import model.User;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

/**
 * Created by Veera on 27.3.2015.
 */
public class CompletelyBlindMatch extends MatcherAbstract implements Matcher {
    @Override

    // Returns a random user from all the users. 75% to get Shaun the Sheep if you aren't VIP
    public User match(User user, HashMap<String, User> userList) {


        random = new Random();

        User shaun = userList.get("shaun");

        matches = new ArrayList<>();
        matches.addAll(userList.values());

        // 75% to get Shaun the Sheep if you aren't VIP!
        double chance = (double) random.nextInt(matches.size()) / (double) matches.size();
        System.out.println(chance);
        if (chance < .75) {
            return shaun;
        }
        /// Remove the searcher from matches.

        super.removeUserFromResults(user, matches);

        // Get random from the matches
        return super.getRandomFromMatches();


    }
}

