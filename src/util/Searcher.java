package util;

import model.User;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Pepe on 26.2.2015.
 */
public class Searcher {




    public static HashMap<String, User> searchbyAgeandGender(User searcher, int age, String gender, HashMap<String, User> userList) {
        HashMap<String, User> matches = new HashMap<String, User>();

        boolean isMatch = false;
        for (Map.Entry<String, User> user : userList.entrySet()) {
            if (user.getValue().getAge() == age && user.getValue().getGender().equals(gender)) {
                matches.put(user.getKey(), user.getValue());
                isMatch = true;

            }

        }

        if (!isMatch) {
            System.out.println("Sorry, no matches found");
            return null;
        }

        if (matches.containsKey(searcher.getUserName()))
        {
            matches.remove(searcher.getUserName());
        }


        return matches;
    }

    /// Works in theory but need to change it to look for hobbies / music etc.
    /*public static HashMap<String, User> searchbyLikes(User searcher, HashMap<String, User> userList) {
        boolean isMatch = false;
        HashMap<String, User> matches = new HashMap<String, User>();

        for (Map.Entry<String, User> user : userList.entrySet()) {
            /// Disjoint checks if both sets contain same values. Returns false if yes, true if no
            if (!Collections.disjoint(searcher.getLikes(), user.getValue().getLikes())) {
                matches.put(user.getKey(), user.getValue());

                isMatch = true;
            }
        }

        if (!isMatch)
        {
            System.out.println("No matches found!");
            return null;
        }


        /// Remove the searcher from matches.
        if (matches.containsKey(searcher.getUserName()))
        {
            matches.remove(searcher.getUserName());
        }

        return matches;
    }
*/
    public static HashMap<String, User> searchByAgeRange(User searcher, int minAge, int maxAge, HashMap<String, User> userList) {


        if (minAge < 18) {
            minAge = 18;
        }


        HashMap<String, User> matches = new HashMap<String, User>();
        boolean isMatch = false;

        for (Map.Entry<String, User> user : userList.entrySet()) {

            if (user.getValue().getAge() >= minAge && user.getValue().getAge() <= maxAge) {

                matches.put(user.getKey(), user.getValue());
                isMatch = true;

            }

        }
        if (!isMatch) {
            System.out.println("Sorry, no matches found!");
            return null;
        }

        if (matches.containsKey(searcher.getUserName()))
        {
            matches.remove(searcher.getUserName());
        }

        return matches;

    }


}


