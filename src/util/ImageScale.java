package util;

import org.apache.commons.io.FileUtils;
import org.imgscalr.Scalr;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Created by extradikke on 14/03/15.
 */
public class ImageScale {

    public static void scale(String fileName) {
        BufferedImage image;
        try {
            System.out.println(FilePaths.TEMP_DIRECTORY + fileName);
            image = ImageIO.read(new File(FilePaths.TEMP_DIRECTORY + fileName));
            BufferedImage resized = Scalr.resize(image, Scalr.Method.QUALITY, 150, 150);
            File mainSave = new File(FilePaths.PICTURES_DIRECTORY_MAIN + fileName);
            File serverSave = new File(FilePaths.PICTURES_DIRECTORY_SERVER + fileName);
            ImageIO.write(resized, "jpeg", mainSave);
            FileUtils.copyFile(mainSave, serverSave);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
