package util;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import model.User;
import tester.Testeroos;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Pepe on 26.2.2015.
 */
public class UserReader {
    File dir;
    JsonReader userReader;
    Gson gson;
    UserLoader userLoader;





    public UserReader() throws IOException {

    }
        public HashMap<String, User> readAllUsers()
        {

          HashMap<String, User> userList = new HashMap<String, User>();

            dir = new File(FilePaths.USER_DIRECTORY);
            gson = new Gson();


            /// Read each JSON-file -> create user -> add to userList
            for (File file : dir.listFiles()) {
                try {
                    userReader = new JsonReader(new FileReader(file));
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    System.out.println("Wrong filepath!");
                }

                userLoader = gson.fromJson(userReader, UserLoader.class);

                User user = new User(userLoader.getUserName(), userLoader.getPassword(), userLoader.getFirstName(), userLoader.getLastName(), userLoader.getEmail(), userLoader.getAge(), userLoader.getlocation(), userLoader.getGender(), userLoader.getInterestedIn(), userLoader.getCreditcard(), userLoader.getHobbies(), userLoader.getMusic(), userLoader.isVIP());
                    //       user.likes = new ArrayList<String>(Arrays.asList(userLoader.getLikes()));
                //  user.dislikes = new ArrayList<String>(Arrays.asList(userLoader.getDislikes()));
//                System.out.println(user.toString());

                userList.put(user.getUserName(), user);
                try {
                    userReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } return userList;
        }


}
