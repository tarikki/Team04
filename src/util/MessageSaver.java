package util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;
import model.MailMessage;
import model.MessageChain;
import model.User;

import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Pepe on 18.3.2015.
 */


/// This class converts messages to JSON format for storage
public class MessageSaver {

    public static void addMessageToChain(String owner, MailMessage message) {
        HashMap<String, MessageChain> messageChains = MessageLoader.loadMessages(owner);

        /** First find out who is the other party of the messages**/
        boolean ownMessage = false;
        String notOwner;
        if (!message.getSender().equals(owner)) {
            notOwner = message.getSender();
            ownMessage = true;
        } else {
            notOwner = message.getReceiver();
        }

        System.out.println("not owner " + notOwner + ", saving for: " + owner + " chainlength: " + messageChains.size());

        /** if the user has communicated with another user before, fetch the old messages**/
        MessageChain messageChainNew = new MessageChain(message, notOwner);
        if (messageChains.containsKey(notOwner)) {
//            if (!messageChains.get(notOwner).getLastMessageDate().equals(message.getTimeStamp()))
            messageChainNew = messageChains.get(notOwner);
            messageChainNew.newMessage(message);
        }

        if (!ownMessage){
            messageChainNew.allMessagesRead();
        }

        messageChains.put(notOwner, messageChainNew);

        saveMessages(owner, messageChains);

    }

    public static void saveMessages(String user, HashMap<String, MessageChain> messageChains) {
        FileWriter fileWriter;
        String USER_MESSAGES = FilePaths.USER_MESSAGE_DIRECTORY + user + "-messages" + ".json";

        /**convert from hashmap to arraylist**/

        ArrayList<MessageChain> messageChainsArrayList = new ArrayList<>();
        for (MessageChain messageChain : messageChains.values()) {
            messageChainsArrayList.add(messageChain);
        }

        Gson gson = new GsonBuilder().setPrettyPrinting().create();

        String jsonRepresentation = gson.toJson(messageChainsArrayList);


        try {
            fileWriter = new FileWriter(USER_MESSAGES); /// If this is true it won't overwrite old content!
            fileWriter.write(jsonRepresentation);
            fileWriter.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
