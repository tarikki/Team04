package util;

import java.nio.file.Paths;
import java.util.regex.Pattern;

/**
 * Created by Pepe on 27.2.2015.
 */
public class FilePaths {


    public static String USER_DIRECTORY;
    public static String PICTURES_DIRECTORY_MAIN;
    private static String WORKING_DIRECTORY;
    private static String DEFAULT_DIRECTORY_NAME = "Users";
    private static String MESSAGES = "Messages";
    public static String USER_MESSAGE_DIRECTORY;
    public static String TEMP_DIRECTORY;
    public static String PICTURES_DIRECTORY_SERVER;
    public static String PICTURES_DIRECTORY_HTML = "../resources/pictures/";


    // Static block so these are correct when program is first started
    static {

//        WORKING_DIRECTORY = getWD();

        // Tariq's directory
//        WORKING_DIRECTORY = "/media/extradikke/UbuntuData/programming/java/dare2date2";
  //      USER_DIRECTORY = "/media/extradikke/UbuntuData/programming/java/dare2date2/" + DEFAULT_DIRECTORY_NAME + osPathCorrection();
  //      PICTURES_DIRECTORY_MAIN = "/media/extradikke/UbuntuData/programming/java/dare2date2/resources/pictures/";
  //      PICTURES_DIRECTORY_SERVER = "/media/extradikke/UbuntuData/programming/java/dare2date2/out/artifacts/dare2date2_war_exploded/resources/pictures/";
  //      TEMP_DIRECTORY = "/media/extradikke/UbuntuData/programming/java/dare2date2/temp/";
  //      USER_MESSAGE_DIRECTORY = "/media/extradikke/UbuntuData/programming/java/dare2date2/Messages/";

        // Pepe's Directory
        USER_DIRECTORY = "C:\\Workspace BU\\Dare2Date\\" + DEFAULT_DIRECTORY_NAME + osPathCorrection();
        USER_MESSAGE_DIRECTORY = "C:\\Workspace BU\\Dare2Date\\" + MESSAGES + osPathCorrection();
        PICTURES_DIRECTORY_MAIN = "C:\\Workspace BU\\Dare2Date\\resources\\pictures\\";
      PICTURES_DIRECTORY_SERVER = "C:\\Workspace BU\\Dare2Date\\out\\artifacts\\Dare2Date_war_exploded\\resources\\pictures\\";
       TEMP_DIRECTORY = "C:\\Workspace BU\\Dare2Date\\temp";

//        System.out.println(USER_DIRECTORY);
        // Birk's Directory

        // Klara's Directory


    }

    public static String getWD() {
        Pattern opSysPattern = Pattern.compile("\\w+");
        String fullPath = "";
        String path = Paths.get(".").toAbsolutePath().normalize().toString();
        String opSys = System.getProperty("os.name").toLowerCase();

        java.util.regex.Matcher matcher = opSysPattern.matcher(opSys);
        if (matcher.find()) {

            opSys = matcher.group();
            if (opSys.equals("linux")) {
//                System.out.println("linux");
                fullPath = path + "/";
            }
            if (opSys.equals("windows")) {
                fullPath = path + "\\";
            }
//            System.out.println(fullPath);
        }
        return fullPath;
    }

    public static String osPathCorrection() {
        Pattern opSysPattern = Pattern.compile("\\w+");
        String opSys = System.getProperty("os.name").toLowerCase();
        java.util.regex.Matcher matcher = opSysPattern.matcher(opSys);
        String result = "";
        if (matcher.find()) {

            opSys = matcher.group();
            if (opSys.equals("linux")) {

                result = "/";
            }
            if (opSys.equals("windows")) {
                result = "\\";
            }

        }
        return result;


    }
}
