package util;

import java.util.Arrays;

/**
 * Created by Pepe on 26.2.2015.
 */
public class UserLoader {

/// This class is used by JSON


    private String userName;
    private String firstName;
    private String lastName;
    private int age;
    private String location;
    private String gender;
    private String email;
    private String interestedIn;
    private String creditcard;
    private String password;
    private boolean VIP;
    private String hobbies;
    private String music;
    public String pictureName;

    public void setCreditcard(String creditcard) {
        this.creditcard = creditcard;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getHobbies() {
        return hobbies;
    }

    public void setHobbies(String hobbies) {
        this.hobbies = hobbies;
    }

    public void setInterestedIn(String interestedIn) {
        this.interestedIn = interestedIn;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getMusic() {
        return music;
    }

    public void setMusic(String music) {
        this.music = music;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isVIP() {
        return VIP;
    }

    public void setVIP(boolean VIP) {
        this.VIP = VIP;
    }

    public String getPassword() {
        return password;
    }

    public String getCreditcard() {
        return creditcard;
    }

    public String getEmail() {
        return email;
    }

    public String getInterestedIn() {
        return interestedIn;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    @Override
    public String toString() {
        return "UserLoader{" +
                "userName='" + userName + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", age=" + age +
                ", location='" + location + '\'' +
                ", gender='" + gender + '\'' +
                ", email='" + email + '\'' +
                ", creditcard='" + creditcard + '\'' +
                ", password='" + password + '\'' +
                ", interestedIn='" + interestedIn + '\'' +

                '}';
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getlocation() {
        return location;
    }

    public void setlocation(String location) {
        this.location = location;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }


}


