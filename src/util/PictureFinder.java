package util;

import java.io.File;
import java.io.FilenameFilter;

/**
 * Created by extradikke on 14/03/15.
 */
public class PictureFinder {

//    private static final String userName;
//
//    static FilenameFilter filenameFilter = new FilenameFilter(String userName) {
//        @Override
//        public boolean accept(File dir, String name) {
//            if (name.toLowerCase().startsWith(userName)) {
//                return true;
//            } else {
//                return false;
//            }
//
//        }
//    };

    public static String getPicturePath(String userName) {
        final String userNamef = userName.toLowerCase();
        String result = "";
        File f = new File(FilePaths.PICTURES_DIRECTORY_SERVER);
        File[] files = f.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                if (name.toLowerCase().startsWith(userNamef)) {
                    return true;
                } else {
                    return false;
                }

            }
        });
        if (files.length != 0) {
            return files[0].getName();
        } else {
            return "defaultprofile.png";
        }


    }
}
