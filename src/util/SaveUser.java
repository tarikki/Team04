package util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import model.User;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Pepe on 27.2.2015.
 */
public class SaveUser

{

    private static String USER_FILE_NAME;
    private static String USER_FINAL_PATH;
    private FileWriter fileWriter;


    public User saveUser(String userName, String password, String firstName, String lastName, String email, int age, String addressCity, String gender, String interestedIn, String creditcard,  String hobbies, String music, boolean VIP, HashMap<String, User> userList) {


        User user = new User(userName, password, firstName, lastName, email, age, addressCity, gender, interestedIn, creditcard, hobbies, music, VIP);
        USER_FILE_NAME = userName + ".json";
        USER_FINAL_PATH = FilePaths.USER_DIRECTORY + USER_FILE_NAME;
        userList.put(userName, user);

        Gson gson = new GsonBuilder().setPrettyPrinting().create();

        String jsonRepresentation = gson.toJson(user);
        System.out.println(jsonRepresentation);

        try {
            fileWriter = new FileWriter(USER_FINAL_PATH);
            fileWriter.write(jsonRepresentation);
            fileWriter.close();

        } catch (IOException e) {
            e.printStackTrace();
        }


//        if (!Test.userList.contains(user)) {
//            Test.userList.add(user);
//        } else {
//            System.out.println("That user already exists!");
//            return null;
//        }

        return user;
    }

    public User editProfile(User user) {



        USER_FILE_NAME = user.getUserName()+ ".json";
        USER_FINAL_PATH = FilePaths.USER_DIRECTORY + USER_FILE_NAME;


        Gson gson = new GsonBuilder().setPrettyPrinting().create();

        String jsonRepresentation = gson.toJson(user);
        System.out.println(jsonRepresentation);

        try {
            fileWriter = new FileWriter(USER_FINAL_PATH);
            fileWriter.write(jsonRepresentation);
            fileWriter.close();

        } catch (IOException e) {
            e.printStackTrace();
        }


//        if (!Test.userList.contains(user)) {
//            Test.userList.add(user);
//        } else {
//            System.out.println("That user already exists!");
//            return null;
//        }

        return user;
    }


}


