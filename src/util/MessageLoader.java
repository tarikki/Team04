package util;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import model.MessageChain;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Pepe on 18.3.2015.
 */
public class MessageLoader {


    public static HashMap<String, MessageChain> loadMessages(String user) {
        ArrayList<MessageChain> messages = new ArrayList<>();
        HashMap<String, MessageChain> messageMap = new HashMap<>();

        Type type = new TypeToken<List<MessageChain>>() {
        }.getType();
        File dir = new File(FilePaths.USER_MESSAGE_DIRECTORY);
        Gson gson = new Gson();
        JsonReader messageLoader = null;


        /// Loop through all files in message directory. If the file matches the username then load it and turn it into a messageChain.
        for (File file : dir.listFiles()) {
            System.out.println(file.toString() + " / " + FilePaths.USER_MESSAGE_DIRECTORY+user + "-messages" + ".json");
            if (file.toString().equals(FilePaths.USER_MESSAGE_DIRECTORY+user + "-messages" + ".json")) {

                System.out.println("loading " + file.toString());
                try {
                    messageLoader = new JsonReader(new FileReader(file));
                    messages = gson.fromJson(messageLoader, type);

                    try {
                        messageLoader.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    System.out.println("Wrong filepath!");
                }
                messageMap = populateHashMap(messages);


            }
        }

        return messageMap;
    }

    public static HashMap<String, MessageChain> populateHashMap(ArrayList<MessageChain> messages) {
        HashMap<String, MessageChain> messageMap = new HashMap<>();
        for (MessageChain messageChain : messages) {
            messageMap.put(messageChain.getNotOwner(), messageChain);
        }


        return messageMap;
    }

    public static String countUnreadMail(String currentUserName) {
        HashMap<String, MessageChain> messageChains = MessageLoader.loadMessages(currentUserName);
        int unreadMessages = 0;
        for (MessageChain messageChain : messageChains.values()) {
            unreadMessages += messageChain.getUnreadMessagesNumber();
        }
        System.out.println("unread messages "+ unreadMessages);
        String unreadString = "";
        if (unreadMessages > 0) {
            unreadString = "(" + String.valueOf(unreadMessages) + ")";
            System.out.println("unreadMessages " + unreadString + " alksdjf");


        }
        System.out.println("unreadMessages " + unreadString + " alksdjf");
        return unreadString;
    }


}



