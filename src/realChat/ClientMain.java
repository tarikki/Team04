package realChat;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.Scanner;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by extradikke on 25/03/15.
 */
public class ClientMain {

    public static void main(String[] args) {


        Scanner scanner=new Scanner(System.in);
        System.out.println("Enter Your name and press Enter:");
        String name=scanner.nextLine().trim();
        System.out.println("Who do you want to talk to?");
        String receiver=scanner.nextLine().trim();
        System.out.println("What IP to connect to? (press enter to connect to server on same machine)");
        String ip=scanner.nextLine().trim();
        if (ip.equals("")){
            ip = "localhost";
        }
//        System.setProperty("java.rmi.server.hostname", ip);
        try {
            ChatInterfaceServer server = (ChatInterfaceServer) Naming.lookup("rmi://localhost:2023/ABC");
            ChatInterFaceClient client = new Client(name, receiver, server);

            server.joinServer(client);
            System.out.println("pass this?");
            while (true) {
                if (scanner.hasNext()) {
                    try {
                        client.treatInput(scanner.nextLine());
                        System.out.print(name+":");
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                } Thread.sleep(500);
            }
        } catch (RemoteException | MalformedURLException | NotBoundException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


    }
}
