package realChat; /**
 * Created by HP on 13.3.2015.
 */


import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Client extends UnicastRemoteObject implements ChatInterFaceClient {

    private String name;
    private String receiver;
    private Pattern pattern = Pattern.compile("(.*):(.*)");
    private ChatInterfaceServer server;


    protected Client(String name, String receiver, ChatInterfaceServer server) throws RemoteException {
        this.name = name;
        this.receiver = receiver;
        this.server = server;
    }

    @Override
    public void treatInput(String input) throws RemoteException {

            server.sendMessageToServer(new Message(input, receiver, name));




    }


    @Override
    public String getName() throws RemoteException {
        return this.name;
    }

    @Override
    public void relayMessageToClient(Message message) throws RemoteException {
        System.out.println(message.getSender() + ": " + message.getContent());

    }


}