package realChat;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Created by extradikke on 25/03/15.
 */
public interface ChatInterFaceClient extends Remote {
    public String getName() throws RemoteException;
    public void relayMessageToClient(Message message) throws RemoteException;
    public void treatInput(String input)throws RemoteException;
}
