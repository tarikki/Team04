package realChat; /**
 * Created by HP on 13.3.2015.
 */

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.HashMap;

public class Server extends UnicastRemoteObject implements ChatInterfaceServer{

    private String name;
    private ArrayList<Message> messages  = new ArrayList<>();
    private HashMap<String, ChatInterFaceClient> clients = new HashMap<>();


    public Server(String n)  throws RemoteException {
        this.name=n;
    }

    @Override
    public String getName() throws RemoteException {
        return null;
    }

    @Override
    public void sendMessageToServer(Message message) throws RemoteException {
        messages.add(message);
    }

    @Override
    public void joinServer(ChatInterFaceClient client) throws RemoteException {
        System.out.println(client.getName() + " has joined");
        for (ChatInterFaceClient clientOld : clients.values()) {
            clientOld.relayMessageToClient(new Message(client.getName() + " has joined", clientOld.getName(), "Server"));
        }
        clients.put(client.getName(), client);
        client.relayMessageToClient(new Message("Welcome to the Server!", client.getName(), "Server"));
    }


    public HashMap<String, ChatInterFaceClient> getClients() {
        return clients;
    }

    public ArrayList<Message> getMessages() {
        return messages;
    }
}