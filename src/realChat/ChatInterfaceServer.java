package realChat; /**
 * Created by HP on 13.3.2015.
 */

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ChatInterfaceServer extends Remote{
    public String getName() throws RemoteException;
    public void sendMessageToServer(Message message) throws RemoteException;
    public void joinServer(ChatInterFaceClient client)throws RemoteException;
}