package realChat;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by extradikke on 25/03/15.
 */
public class ServerMain {

    public static void main(String[] args) {
        try {
//            System.setProperty( "java.rmi.server.hostname", "192.168.RMIServer.IP" ) ;
            final Server server = new Server("mah Server");

//            System.setProperty("java.rmi.server.hostname", "192.168.0.17");
            LocateRegistry.createRegistry(2023);


            Naming.rebind("rmi://localhost:2023/ABC", server);

            System.out.println(LocateRegistry.getRegistry(2023));
            System.out.println("Waiting for clients");
            Timer timer = new Timer();
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    if (!server.getMessages().isEmpty()) {
                        for (Iterator<Message> iterator = server.getMessages().iterator(); iterator.hasNext(); ) {
                            Message message = iterator.next();
                            if (server.getClients().containsKey(message.getReceiver())) {
                                try {
                                    server.getClients().get(message.getReceiver()).relayMessageToClient(message);
                                    System.out.println("Sent message from " + message.getSender() + " to " + message.getReceiver());
                                    iterator.remove();
                                } catch (RemoteException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }
                }
            }, 500, 500);
        } catch (RemoteException | MalformedURLException e) {
            e.printStackTrace();
        }
    }
}
